package com.logisfera.devTest.utils;

import javafx.util.Pair;

import java.util.Comparator;

/**
 * @author Tomasz Cybulski
 * @created 2020-08-11
 */
public class DataComparator implements Comparator<Pair<Integer, Integer>> {
    
    @Override
    public int compare(Pair<Integer, Integer> o1, Pair<Integer, Integer> o2) {
        int result = compare(o1.getKey(), o2.getKey());

        if( result == 0 ) {
            result = compare(o2.getValue(), o2.getValue());
        }

        return result;
    }

    private int compare(Integer val1, Integer val2) {
        return val1.compareTo(val2);
    }
}