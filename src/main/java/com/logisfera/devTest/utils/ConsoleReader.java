package com.logisfera.devTest.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Tomasz Cybulski
 * @created 2020-08-11
 */
public class ConsoleReader {


    public String readLine() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        return br.readLine().trim();
    }

    public List<String> readMultipleLine() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        DataParser dataParser = new DataParser();

        List<String> allLinesList = new ArrayList<>();
        List<Integer> firstLine = dataParser.changeStringToIntegerList(br.readLine());
        if (!(firstLine.isEmpty() || firstLine.size() > 1)) {
            for (int i = 0; i < firstLine.get(0); i++) {
                allLinesList.add(br.readLine());
            }
        }
        return allLinesList;
    }

}
