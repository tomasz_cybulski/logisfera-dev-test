package com.logisfera.devTest.utils;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Tomasz Cybulski
 * @created 2020-08-11
 */
public class DataParser {

    public int changeStringToInt(String str) {
        if (isIntegerParseInt(str)) {
            return Integer.parseInt(str);
        } else {
            return 0;
        }
    }

    public List<Integer> changeStringToIntegerList(String str) {
        return Arrays.stream(str.trim().split("\\s+"))
                .filter(DataParser::isIntegerParseInt)
                .map(Integer::parseInt)
                .collect(Collectors.toList());
    }

    public List<Pair<Integer, Integer>> changeStingToIntegerPairList(List<String> strings) {
        List<Pair<Integer, Integer>> pairList = new ArrayList<>();
        strings.forEach(element -> {
            List<Integer> pairElements = changeStringToIntegerList(element);
            if (pairElements.size() == 2) {
                Collections.sort(pairElements);
                pairList.add(new Pair<>(pairElements.get(0), pairElements.get(1)));
            }
        });
        return pairList;
    }

    private static boolean isIntegerParseInt(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
