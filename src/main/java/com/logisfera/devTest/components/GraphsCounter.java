package com.logisfera.devTest.components;

import com.logisfera.devTest.utils.DataComparator;
import javafx.util.Pair;

import java.util.List;

/**
 * @author Tomasz Cybulski
 * @created 2020-08-11
 */
public class GraphsCounter {

    public void countSeparatedGraph(List<Pair<Integer, Integer>> integerPairList) {
        integerPairList.sort(new DataComparator());
        int graphCounter = integerPairList.size() > 0 ? 1 : 0;

        for (int i = 0; i < integerPairList.size(); i++) {
            Pair<Integer, Integer> firstPair = integerPairList.get(i);
            Pair<Integer, Integer> secondPair = null;
            if (integerPairList.size() > i + 1) {
                secondPair = integerPairList.get(++i);
            }

            if (secondPair != null && firstPair.getValue().compareTo(secondPair.getKey()) < 0) {
                graphCounter++;
            }
        }
        System.out.println("Separated graphs counted: " + graphCounter );
    }
}
