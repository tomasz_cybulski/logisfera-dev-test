package com.logisfera.devTest.components;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * @author Tomasz Cybulski
 * @created 2020-08-11
 */
public class DistinctElementProvider {

    private static final Logger logger = LoggerFactory.getLogger(DistinctElementProvider.class);

    public void sortListOfIntegersWithAddedInfo(List<Integer> integerList) {
        Set<Integer> integerSet = new TreeSet<>(integerList);

        String integerSetString = integerSet.stream().map(Object::toString).collect(Collectors.joining(" "));

        System.out.println(integerSetString);
        System.out.println("count: " + integerList.size());
        System.out.println("distinct: " + integerSet.size());
        System.out.println("min: " + integerList.stream().min(Integer::compareTo).orElseGet(() -> {
            logger.warn("Object not available");
            return null;
        }));
        System.out.println("max: " + integerList.stream().max(Integer::compareTo).orElseGet(() -> {
            logger.warn("Object not available");
            return null;
        }));
    }
}
