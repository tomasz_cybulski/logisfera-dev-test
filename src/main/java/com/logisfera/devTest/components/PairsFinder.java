package com.logisfera.devTest.components;

import com.logisfera.devTest.utils.DataComparator;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Tomasz Cybulski
 * @created 2020-08-11
 */
public class PairsFinder {

    private static final int MINUED = 13;

    public void providePairsThatSumIsEqualThirteen(List<Integer> integerList) {
        List<Pair<Integer, Integer>> pairList = new ArrayList<>();

        Iterator<Integer> iterator = integerList.iterator();
        while (iterator.hasNext()) {
            List<Integer> integers = new ArrayList<>();
            int subtrahend = iterator.next();
            int difference = MINUED - subtrahend;
            if (difference >= 0) {
                integers = integerList.stream().filter(var -> var == difference).collect(Collectors.toList());
            }
            if (!integers.isEmpty()) {
                integers.forEach(element1 -> {
                    if (subtrahend > element1) {
                        pairList.add(new Pair<>(element1, subtrahend));
                    } else {
                        pairList.add(new Pair<>(subtrahend, element1));
                    }
                });
            }
            iterator.remove();
        }
        pairList.sort(new DataComparator());

        String pairListString = pairList.size() > 0 ? "Pairs list:\n" + pairList.stream()
                .map(Object::toString)
                .map(element -> element.replace("=", " "))
                .collect(Collectors.joining("\n")) : "No pair is available";

        System.out.println(pairListString);
    }

}
