package com.logisfera.devTest;


import com.logisfera.devTest.components.DistinctElementProvider;
import com.logisfera.devTest.components.PairsFinder;
import com.logisfera.devTest.components.GraphsCounter;
import com.logisfera.devTest.utils.ConsoleReader;
import com.logisfera.devTest.utils.DataParser;
import javafx.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

/**
 * @author Tomasz Cybulski
 * @created 2020-08-11
 */
public class App {

    public static void main(String[] args) {

        final Logger logger = LoggerFactory.getLogger(DistinctElementProvider.class);
        ConsoleReader consoleReader = new ConsoleReader();
        DataParser dataParser = new DataParser();

        try {
            boolean runApp = true;
            while (runApp) {
                System.out.println("Choose from these choices");
                System.out.println("1 - Run distinct element provider");
                System.out.println("2 - Run pairs that sum is equal 13 finder");
                System.out.println("3 - Run graph counter");
                System.out.println("4 - Quit");

                int choice = dataParser.changeStringToInt(consoleReader.readLine());

                switch (choice) {
                    case 1:
                        DistinctElementProvider elementProvider = new DistinctElementProvider();
                        System.out.println("Give me a list of integers and press Enter:");
                        String readLine = consoleReader.readLine();
                        List<Integer> integerList = dataParser.changeStringToIntegerList(readLine);
                        elementProvider.sortListOfIntegersWithAddedInfo(integerList);
                        break;
                    case 2:
                        PairsFinder pairsFinder = new PairsFinder();
                        System.out.println("Give me a list of integers and press Enter:");
                        String read = consoleReader.readLine();
                        List<Integer> integers = dataParser.changeStringToIntegerList(read);
                        pairsFinder.providePairsThatSumIsEqualThirteen(integers);
                        break;
                    case 3:
                        GraphsCounter graphsCounter = new GraphsCounter();
                        System.out.println("Give me a number of lines to read, press Enter and write lines of integer pair x y:");
                        List<String> readLines = consoleReader.readMultipleLine();
                        List<Pair<Integer, Integer>> integerPairList = dataParser.changeStingToIntegerPairList(readLines);
                        graphsCounter.countSeparatedGraph(integerPairList);
                        break;
                    case 4:
                        System.out.println("Exiting application...");
                        runApp = false;
                    default:
                        System.out.println("Not a valid Menu Option! Please Select Another.");
                }
            }
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
    }
}
